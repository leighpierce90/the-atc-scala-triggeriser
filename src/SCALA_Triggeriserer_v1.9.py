#!/usr/bin/env Python3 
#https://repl.it/@PySimpleGUI/PySimpleGUIWeb-Demos#main.py     
import PySimpleGUI as sg  
import socket

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

#set ipaddress vars
RR150 = '192.168.48.33'   #150M LED
RRWP = '192.168.48.35'
RRCUBA = '192.168.48.50'
RRCUBB = '192.168.48.51'
RRTOTH = '192.168.48.34'
RRMark = '192.168.48.35'
RRMemA = '192.168.48.23'  # Triple Crown
# RRPubA = '192.168.48.29'  # Skyline
RRPubB = '192.168.48.12'  # Kenso 2
RRPubC = '192.168.48.13'  # Kenso 3
RRPubD = '192.168.48.14'  # Late Mail 2
RRPubE = '192.168.48.15'  # Late Mail 3
RRPubF = '192.168.48.16'  # Late Mail 4
RRPubG = '192.168.48.17'  # Silks 1
RRPubH = '192.168.48.18'  # Silks 2
RRPubI = '192.168.48.19'  # Silks 3
RRPubJ = '192.168.48.20'  # Silks 4
RRPubK = '192.168.48.27'  # Centennial
RRMemB = '192.168.48.39'  # Villiers
RRMemC = '192.168.48.40'  # Doncaster
RRMemD = '192.168.48.29'  # Skyline

RG150 = '192.168.89.60'   #150M LED
RGWP = '192.168.89.59'    #Winning Post
RGMark = '192.168.89.61'  #Marketing Loop
RGMemA = '192.168.89.13'  # Winners Circle Bar
RGMemB = '192.168.89.15'  # Winners Circle Snack
RGMemC = '192.168.89.28'  # Tulloch Bar
RGMemD = '192.168.89.30'  # Tulloch Snack
RGMemE = '192.168.89.44'  # Moet Bar
RGPubA = '192.168.89.17'  # Belle Du Jour 1
RGPubB = '192.168.89.18'  # Belle Du Jour 2
RGPubC = '192.168.89.20'  # Jim Beam 1
RGPubD = '192.168.89.21'  # Jim Beam 2
RGPubE = '192.168.89.23'  # Legends 1
RGPubF = '192.168.89.24'  # Legends 2
RGPubG = '192.168.89.26'  # Running Rails Snack
RGPubH = '192.168.89.41'  # Cocktail Bar 1
RGPubI = '192.168.89.42'  # Cocktail Bar 2


layout = [    

#Location selection
  [sg.Frame(layout=[ 
  [sg.Radio('Randwick', "RADIO1", key=('-Randwick-'), size=(10,1),  default=True), sg.Radio('Rosehill', "RADIO1", key=('-Rosehill-'))],     
  [sg.Checkbox('150 LED', key=('-150-')), sg.Checkbox('WP LED', key=('-WP-')), sg.Checkbox('Cubes', key=('-Cubes-')), sg.Checkbox('TOTH', key=('-TOTH-')), sg.Checkbox('Marketing', key=('-Market-'))],
  [sg.Checkbox('Members Adverts', key=('-MemA-')), sg.Checkbox('Public Adverts', key=('-PubA-'))]], title='Location',title_color='#ff9e9d', relief=sg.RELIEF_SUNKEN, tooltip='Use these to set what you want to trigger')],  
  [sg.Text('_'  * 67)], 

#One off triggers
  [sg.Text('Ad hoc triggers')],      
  [sg.InputText('Trigger', key=('-Text-'))],
  [sg.Radio('Boolean', "RADIO2", key=('-Boolean-'), size=(8,1), default=True), sg.Button('Start'), sg.Button('Stop'), sg.Radio('Integer', "RADIO2", key=('-Integer-')), sg.Submit(button_text='Go',tooltip='Send Trigger'), sg.Button('Clear')],           
  [sg.Text('_'  * 67)], 

#Updates
  [sg.Frame(layout=[ 
  [sg.Text('Variable')],      
  [sg.Radio('PotY', "RADIO5", key=('-R1U-'), default=True), sg.Radio('Custom (Type Below)', "RADIO5", key=('-R2U-'))],
  [sg.InputText('Variable', key=('-TextVar-'))], 
  [sg.Text('Message')], 
  [sg.InputText('Message', key=('-TextMes-'))],
  ], title='Text Updates',title_color='#ff9e9d', relief=sg.RELIEF_SUNKEN, tooltip='Use these to select triggers')], 
  [sg.Submit(button_text='Update',tooltip='Send Trigger')],   
  [sg.Text('_'  * 67)], 

#Boolean Triggers 
  #Race Statics
  [sg.Frame(layout=[          
  [sg.Radio('R1 Static', "RADIO3", key=('-R1S-')), sg.Radio('R2 Static', "RADIO3", key=('-R2S-')), sg.Radio('R3 Static', "RADIO3", key=('-R3S-')), sg.Radio('R4 Static', "RADIO3", key=('-R4S-')), sg.Radio('R5 Static', "RADIO3", key=('-R5S-'))],
  [sg.Radio('R6 Static', "RADIO3", key=('-R6S-')), sg.Radio('R7 Static', "RADIO3", key=('-R7S-')), sg.Radio('R8 Static', "RADIO3", key=('-R8S-')), sg.Radio('R9 Static', "RADIO3", key=('-R9S-')), sg.Radio('R10 Static', "RADIO3", key=('-R10S-'))],
  #Others
  [sg.Radio('Pick of the Yard', "RADIO3", key=('-PotY-')), sg.Radio('End of Day', "RADIO3", key=('-EndDay-')), sg.Radio('Track Condition', "RADIO3", key=('-Track-')), sg.Radio('Test Grid', "RADIO3", key=('-TestGrid-'))], 
  #Race Info
  [sg.Radio('Boolean 1', "RADIO3", key=('-B1-')), sg.Radio('Boolean 2', "RADIO3", key=('-B2-')), sg.Radio('Boolean 3', "RADIO3", key=('-B3-')), sg.Radio('Boolean 4', "RADIO3", key=('-B4-'))],
    
  #Finish the section
  ], title='Boolean Triggers',title_color='#ff9e9d', relief=sg.RELIEF_SUNKEN, tooltip='Use these to select triggers')],
  [sg.Button('Start!'), sg.Button('Stop!')],    
  [sg.Text('_'  * 67)],

#Integer Triggers  
  [sg.Frame(layout=[      
  [sg.Radio('PoTH', "RADIO4", key=('-PoTH-')), sg.Radio('Marketing', "RADIO4", key=('-Marketing-')),sg.Radio('Bowermans CW', "RADIO4", key=('-BCW-')), sg.Radio('ESS CW', "RADIO4", key=('-ESSCW-')), sg.Radio('TVC', "RADIO4", key=('-TVC-'))], 
  [sg.Radio('Day Hype', "RADIO4", key=('-DayHype-')), sg.Radio('FS Aspirational', "RADIO4", key=('-FSA-')), sg.Radio('FS 1 Hour', "RADIO4", key=('-FS1-')), sg.Radio('FS 2 Hour', "RADIO4", key=('-FS2-'))],
  [sg.Radio('Trigger 1', "RADIO4", key=('-T1-')), sg.Radio('Trigger 2', "RADIO4", key=('-T2-')), sg.Radio('Trigger 3', "RADIO4", key=('-T3-')), sg.Radio('Trigger 4', "RADIO4", key=('-T4-'))]
  #Finish the section
 ], title='Integer Triggers',title_color='#ff9e9d', relief=sg.RELIEF_SUNKEN, tooltip='Use these to select triggers')],       
 [sg.Button('Go!')] ,
]      

#Set up the window
window = sg.Window('SCALA Triggeriserer v1.9', layout, grab_anywhere=False)

#Let's make everything do stuff in IFTT ways
while True:
  event, values = window.read() 
  if event == sg.WIN_CLOSED or event == 'Exit':
    break    
  #Ad Hoc Triggers
  if event == 'Start':
    AVariable = values['-Text-']
    window['-Boolean-'].Widget.config(fg='#96ff8c')
    #Randwick
    if values['-Randwick-'] == True:
      if values['-150-'] == True:
        sock.sendto(bytes(f'SCMD set {AVariable}=True\x0a', 'utf-8'), (RR150, 7700))
      if values['-WP-'] == True:
        sock.sendto(bytes(f'SCMD set {AVariable}=True\x0a', 'utf-8'), (RRWP, 7700))
      if values['-Cubes-'] == True:
        sock.sendto(bytes(f'SCMD set {AVariable}=True\x0a', 'utf-8'), (RRCUBA, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=True\x0a', 'utf-8'), (RRCUBB, 7700))
      if values['-TOTH-'] == True:
        sock.sendto(bytes(f'SCMD set {AVariable}=True\x0a', 'utf-8'), (RRTOTH, 7700))
      if values['-Market-'] == True:
        sock.sendto(bytes(f'SCMD set {AVariable}=True\x0a', 'utf-8'), (RRMark, 7700))
      if values['-MemA-'] == True:
        sock.sendto(bytes(f'SCMD set {AVariable}=True\x0a', 'utf-8'), (RRMemA, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=True\x0a', 'utf-8'), (RRMemB, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=True\x0a', 'utf-8'), (RRMemC, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=True\x0a', 'utf-8'), (RRMemD, 7700))
      if values['-PubA-'] == True:
#        sock.sendto(bytes(f'SCMD set {AVariable}=True\x0a', 'utf-8'), (RRPubA, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=True\x0a', 'utf-8'), (RRPubB, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=True\x0a', 'utf-8'), (RRPubC, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=True\x0a', 'utf-8'), (RRPubD, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=True\x0a', 'utf-8'), (RRPubE, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=True\x0a', 'utf-8'), (RRPubF, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=True\x0a', 'utf-8'), (RRPubG, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=True\x0a', 'utf-8'), (RRPubH, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=True\x0a', 'utf-8'), (RRPubI, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=True\x0a', 'utf-8'), (RRPubJ, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=True\x0a', 'utf-8'), (RRPubK, 7700))
    #Rosehill
    if values['-Rosehill-'] == True:
      if values['-150-'] == True:
        sock.sendto(bytes(f'SCMD set {AVariable}=True\x0a', 'utf-8'), (RG150, 7700))
      if values['-WP-'] == True:
        sock.sendto(bytes(f'SCMD set {AVariable}=True\x0a', 'utf-8'), (RGWP, 7700))
      if values['-Market-'] == True:
        sock.sendto(bytes(f'SCMD set {AVariable}=True\x0a', 'utf-8'), (RGMark, 7700))
      if values['-MemA-'] == True:
        sock.sendto(bytes(f'SCMD set {AVariable}=True\x0a', 'utf-8'), (RGMemA, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=True\x0a', 'utf-8'), (RGMemB, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=True\x0a', 'utf-8'), (RGMemC, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=True\x0a', 'utf-8'), (RGMemD, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=True\x0a', 'utf-8'), (RGMemE, 7700))
      if values['-PubA-'] == True:
        sock.sendto(bytes(f'SCMD set {AVariable}=True\x0a', 'utf-8'), (RGPubA, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=True\x0a', 'utf-8'), (RGPubB, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=True\x0a', 'utf-8'), (RGPubC, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=True\x0a', 'utf-8'), (RGPubD, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=True\x0a', 'utf-8'), (RGPubE, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=True\x0a', 'utf-8'), (RGPubF, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=True\x0a', 'utf-8'), (RGPubG, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=True\x0a', 'utf-8'), (RGPubH, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=True\x0a', 'utf-8'), (RGPubI, 7700))
  if event == 'Stop':
    AVariable = values['-Text-']
    window['-Boolean-'].Widget.config(fg='white'),
    #Randwick
    if values['-Randwick-'] == True:
      if values['-150-'] == True:
        sock.sendto(bytes(f'SCMD set {AVariable}=False\x0a', 'utf-8'), (RR150, 7700))
      if values['-WP-'] == True:
        sock.sendto(bytes(f'SCMD set {AVariable}=False\x0a', 'utf-8'), (RRWP, 7700))
      if values['-Cubes-'] == True:
        sock.sendto(bytes(f'SCMD set {AVariable}=False\x0a', 'utf-8'), (RRCUBA, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=False\x0a', 'utf-8'), (RRCUBB, 7700))
      if values['-TOTH-'] == True:
        sock.sendto(bytes(f'SCMD set {AVariable}=False\x0a', 'utf-8'), (RRTOTH, 7700))
      if values['-Market-'] == True:
        sock.sendto(bytes(f'SCMD set {AVariable}=False\x0a', 'utf-8'), (RRMark, 7700))
      if values['-MemA-'] == True:
        sock.sendto(bytes(f'SCMD set {AVariable}=False\x0a', 'utf-8'), (RRMemA, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=False\x0a', 'utf-8'), (RRMemB, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=False\x0a', 'utf-8'), (RRMemC, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=False\x0a', 'utf-8'), (RRMemD, 7700))
      if values['-PubA-'] == True:
#        sock.sendto(bytes(f'SCMD set {AVariable}=False\x0a', 'utf-8'), (RRPubA, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=False\x0a', 'utf-8'), (RRPubB, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=False\x0a', 'utf-8'), (RRPubC, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=False\x0a', 'utf-8'), (RRPubD, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=False\x0a', 'utf-8'), (RRPubE, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=False\x0a', 'utf-8'), (RRPubF, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=False\x0a', 'utf-8'), (RRPubG, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=False\x0a', 'utf-8'), (RRPubH, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=False\x0a', 'utf-8'), (RRPubI, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=False\x0a', 'utf-8'), (RRPubJ, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=False\x0a', 'utf-8'), (RRPubK, 7700))

    #Rosehill    
    if values['-Rosehill-'] == True:
      if values['-150-'] == True:
        sock.sendto(bytes(f'SCMD set {AVariable}=False\x0a', 'utf-8'), (RG150, 7700))
      if values['-WP-'] == True:
        sock.sendto(bytes(f'SCMD set {AVariable}=False\x0a', 'utf-8'), (RGWP, 7700))
      if values['-Market-'] == True:
        sock.sendto(bytes(f'SCMD set {AVariable}=False\x0a', 'utf-8'), (RGMark, 7700))
      if values['-MemA-'] == True:
        sock.sendto(bytes(f'SCMD set {AVariable}=False\x0a', 'utf-8'), (RGMemA, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=False\x0a', 'utf-8'), (RGMemB, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=False\x0a', 'utf-8'), (RGMemC, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=False\x0a', 'utf-8'), (RGMemD, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=False\x0a', 'utf-8'), (RGMemE, 7700))
      if values['-PubA-'] == True:
        sock.sendto(bytes(f'SCMD set {AVariable}=False\x0a', 'utf-8'), (RGPubA, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=False\x0a', 'utf-8'), (RGPubB, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=False\x0a', 'utf-8'), (RGPubC, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=False\x0a', 'utf-8'), (RGPubD, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=False\x0a', 'utf-8'), (RGPubE, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=False\x0a', 'utf-8'), (RGPubF, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=False\x0a', 'utf-8'), (RGPubG, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=False\x0a', 'utf-8'), (RGPubH, 7700))
        sock.sendto(bytes(f'SCMD set {AVariable}=False\x0a', 'utf-8'), (RGPubI, 7700))

  if event == 'Go':
    AVariable = values['-Text-']
    if values['-Randwick-'] == True:
      if values['-150-'] == True:
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RR150, 7700))
      if values['-WP-'] == True:
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRWP, 7700))
      if values['-Cubes-'] == True:
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRCUBA, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRCUBB, 7700))
      if values['-TOTH-'] == True:
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRTOTH, 7700))
      if values['-Market-'] == True:
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRMark, 7700))
      if values['-MemA-'] == True:
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRMemA, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRMemB, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRMemC, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRMemD, 7700))
      if values['-PubA-'] == True:
#        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRPubA, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRPubB, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRPubC, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRPubD, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRPubE, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRPubF, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRPubG, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRPubH, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRPubI, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRPubJ, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRPubK, 7700))

    #Rosehill    
    if values['-Rosehill-'] == True:
      if values['-150-'] == True:
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RG150, 7700))
      if values['-WP-'] == True:
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RGWP, 7700))
      if values['-Market-'] == True:
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RGMark, 7700))
      if values['-MemA-'] == True:
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RGMemA, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RGMemB, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RGMemC, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RGMemD, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RGMemE, 7700))
      if values['-PubA-'] == True:
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RGPubA, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RGPubB, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RGPubC, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RGPubD, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RGPubE, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RGPubF, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RGPubG, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RGPubH, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RGPubI, 7700))

  if event == 'Clear':
    AVariable = 'Clear'
    #Randwick
    if values['-Randwick-'] == True:
      if values['-150-'] == True:
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RR150, 7700))
      if values['-WP-'] == True:
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRWP, 7700))
      if values['-Cubes-'] == True:
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRCUBA, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRCUBB, 7700))
      if values['-TOTH-'] == True:
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRTOTH, 7700))
      if values['-Market-'] == True:
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRMark, 7700))
      if values['-MemA-'] == True:
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRMemA, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRMemB, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRMemC, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRMemD, 7700))
      if values['-PubA-'] == True:
#        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRPubA, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRPubB, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRPubC, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRPubD, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRPubE, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRPubF, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRPubG, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRPubH, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRPubI, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRPubJ, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RRPubK, 7700))

    #Rosehill    
    if values['-Rosehill-'] == True:
      if values['-150-'] == True:
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RG150, 7700))
      if values['-WP-'] == True:
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RGWP, 7700))
      if values['-Market-'] == True:
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RGMark, 7700))
      if values['-MemA-'] == True:
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RGMemA, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RGMemB, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RGMemC, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RGMemD, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RGMemE, 7700))
      if values['-PubA-'] == True:
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RGPubA, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RGPubB, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RGPubC, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RGPubD, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RGPubE, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RGPubF, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RGPubG, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RGPubH, 7700))
        sock.sendto(bytes(f'SCMD increment {AVariable}\x0a', 'utf-8'), (RGPubI, 7700))
  

#Text Updates   
    VVariable = 'PickYard'

  if event == 'Update':
    TextMess = values['-TextMes-'] 
    MVariable = ("'{}'".format(TextMess))
    if values['-R1U-'] == True:
      VVariable = 'PickYard'
    if values['-R2U-'] == True:
      VVariable = values['-TextVar-'] 
                            
    window['-Boolean-'].Widget.config(fg='white'),
    #Randwick
    if values['-Randwick-'] == True:
      if values['-150-'] == True:
        sock.sendto(bytes(f'SCMD set {VVariable}={MVariable}\x0a', 'utf-8'), (RR150, 7700))
      if values['-WP-'] == True:
        sock.sendto(bytes(f'SCMD set {VVariable}={MVariable}\x0a', 'utf-8'), (RRWP, 7700))
      if values['-Cubes-'] == True:
        sock.sendto(bytes(f'SCMD set {VVariable}={MVariable}\x0a', 'utf-8'), (RRCUBA, 7700))
        sock.sendto(bytes(f'SCMD set {VVariable}={MVariable}\x0a', 'utf-8'), (RRCUBB, 7700))
      if values['-TOTH-'] == True:
        sock.sendto(bytes(f'SCMD set {VVariable}={MVariable}\x0a', 'utf-8'), (RRTOTH, 7700))
      if values['-Market-'] == True:
        sock.sendto(bytes(f'SCMD set {VVariable}={MVariable}\x0a', 'utf-8'), (RRMark, 7700))
      if values['-MemA-'] == True:
        sock.sendto(bytes(f'SCMD set {VVariable}={MVariable}\x0a', 'utf-8'), (RRMemA, 7700))
        sock.sendto(bytes(f'SCMD set {VVariable}={MVariable}\x0a', 'utf-8'), (RRMemB, 7700))
        sock.sendto(bytes(f'SCMD set {VVariable}={MVariable}\x0a', 'utf-8'), (RRMemC, 7700))
        sock.sendto(bytes(f'SCMD set {VVariable}={MVariable}\x0a', 'utf-8'), (RRMemD, 7700))
      if values['-PubA-'] == True:
#        sock.sendto(bytes(f'SCMD set {VVariable}={MVariable}\x0a', 'utf-8'), (RRPubA, 7700))
        sock.sendto(bytes(f'SCMD set {VVariable}={MVariable}\x0a', 'utf-8'), (RRPubB, 7700))
        sock.sendto(bytes(f'SCMD set {VVariable}={MVariable}\x0a', 'utf-8'), (RRPubC, 7700))
        sock.sendto(bytes(f'SCMD set {VVariable}={MVariable}\x0a', 'utf-8'), (RRPubD, 7700))
        sock.sendto(bytes(f'SCMD set {VVariable}={MVariable}\x0a', 'utf-8'), (RRPubE, 7700))
        sock.sendto(bytes(f'SCMD set {VVariable}={MVariable}\x0a', 'utf-8'), (RRPubF, 7700))
        sock.sendto(bytes(f'SCMD set {VVariable}={MVariable}\x0a', 'utf-8'), (RRPubG, 7700))
        sock.sendto(bytes(f'SCMD set {VVariable}={MVariable}\x0a', 'utf-8'), (RRPubH, 7700))
        sock.sendto(bytes(f'SCMD set {VVariable}={MVariable}\x0a', 'utf-8'), (RRPubI, 7700))
        sock.sendto(bytes(f'SCMD set {VVariable}={MVariable}\x0a', 'utf-8'), (RRPubJ, 7700))
        sock.sendto(bytes(f'SCMD set {VVariable}={MVariable}\x0a', 'utf-8'), (RRPubK, 7700))

    #Rosehill    
    if values['-Rosehill-'] == True:
      if values['-150-'] == True:
        sock.sendto(bytes(f'SCMD set {VVariable}={MVariable}\x0a', 'utf-8'), (RG150, 7700))
      if values['-WP-'] == True:
        sock.sendto(bytes(f'SCMD set {VVariable}={MVariable}\x0a', 'utf-8'), (RGWP, 7700))
      if values['-Market-'] == True:
        sock.sendto(bytes(f'SCMD set {VVariable}={MVariable}\x0a', 'utf-8'), (RGMark, 7700))
      if values['-MemA-'] == True:
        sock.sendto(bytes(f'SCMD set {VVariable}={MVariable}\x0a', 'utf-8'), (RGMemA, 7700))
        sock.sendto(bytes(f'SCMD set {VVariable}={MVariable}\x0a', 'utf-8'), (RGMemB, 7700))
        sock.sendto(bytes(f'SCMD set {VVariable}={MVariable}\x0a', 'utf-8'), (RGMemC, 7700))
        sock.sendto(bytes(f'SCMD set {VVariable}={MVariable}\x0a', 'utf-8'), (RGMemD, 7700))
        sock.sendto(bytes(f'SCMD set {VVariable}={MVariable}\x0a', 'utf-8'), (RGMemE, 7700))
      if values['-PubA-'] == True:
        sock.sendto(bytes(f'SCMD set {VVariable}={MVariable}\x0a', 'utf-8'), (RGPubA, 7700))
        sock.sendto(bytes(f'SCMD set {VVariable}={MVariable}\x0a', 'utf-8'), (RGPubB, 7700))
        sock.sendto(bytes(f'SCMD set {VVariable}={MVariable}\x0a', 'utf-8'), (RGPubC, 7700))
        sock.sendto(bytes(f'SCMD set {VVariable}={MVariable}\x0a', 'utf-8'), (RGPubD, 7700))
        sock.sendto(bytes(f'SCMD set {VVariable}={MVariable}\x0a', 'utf-8'), (RGPubE, 7700))
        sock.sendto(bytes(f'SCMD set {VVariable}={MVariable}\x0a', 'utf-8'), (RGPubF, 7700))
        sock.sendto(bytes(f'SCMD set {VVariable}={MVariable}\x0a', 'utf-8'), (RGPubG, 7700))
        sock.sendto(bytes(f'SCMD set {VVariable}={MVariable}\x0a', 'utf-8'), (RGPubH, 7700))
        sock.sendto(bytes(f'SCMD set {VVariable}={MVariable}\x0a', 'utf-8'), (RGPubI, 7700))
        
        
  #Boolean Triggers
  # Set the trigger name and change text colour to let us know that the trigger is currently enabled
  if event == 'Start!':
    if values['-R1S-'] == True:
      #window.FindElement('-R1S-').Update(text_color = '#96ff8c'),
      window['-R1S-'].Widget.config(fg='#96ff8c'),
      BVariable = 'Race1'
    if values['-R2S-'] == True:
      window['-R2S-'].Widget.config(fg='#96ff8c'),
      BVariable = 'Race2'
    if values['-R3S-'] == True:
      window['-R3S-'].Widget.config(fg='#96ff8c'),
      BVariable = 'Race3'
    if values['-R4S-'] == True:
      window['-R4S-'].Widget.config(fg='#96ff8c'),
      BVariable = 'Race4'
    if values['-R5S-'] == True:
      window['-R5S-'].Widget.config(fg='#96ff8c'),
      BVariable = 'Race5'
    if values['-R6S-'] == True:
      window['-R6S-'].Widget.config(fg='#96ff8c'),
      BVariable = 'Race6'
    if values['-R7S-'] == True:
      window['-R7S-'].Widget.config(fg='#96ff8c'),
      BVariable = 'Race7'
    if values['-R8S-'] == True:
      window['-R8S-'].Widget.config(fg='#96ff8c'),
      BVariable = 'Race8'
    if values['-R9S-'] == True:
      window['-R9S-'].Widget.config(fg='#96ff8c'),
      BVariable = 'Race9'
    if values['-R10S-'] == True:
      window['-R10S-'].Widget.config(fg='#96ff8c'),
      BVariable = 'Race10'
    if values['-B1-'] == True:
      window['-B1-'].Widget.config(fg='#96ff8c'),
      BVariable = 'Boolean1'
    if values['-B2-'] == True:
      window['-B2-'].Widget.config(fg='#96ff8c'),
      BVariable = 'Boolean2'
    if values['-B3-'] == True:
      window['-B3-'].Widget.config(fg='#96ff8c'),
      BVariable = 'Boolean3'
    if values['-B4-'] == True:
      window['-B4-'].Widget.config(fg='#96ff8c'),
      BVariable = 'Boolean4'
    if values['-PotY-'] == True:
      window['-PotY-'].Widget.config(fg='#96ff8c'),
      BVariable = 'YardPick'
    if values['-EndDay-'] == True:
      window['-EndDay-'].Widget.config(fg='#96ff8c'),
      BVariable = 'EndDay'
    if values['-Track-'] == True:
      window['-Track-'].Widget.config(fg='#96ff8c'),
      BVariable = 'Track'
    if values['-TestGrid-'] == True:
      window['-TestGrid-'].Widget.config(fg='#96ff8c'),
      BVariable = 'TestGrid'

    #Now things get annoying, send the trigger to the right IP address
    #Randwick
    if values['-Randwick-'] == True:
      if values['-150-'] == True:
        sock.sendto(bytes(f'SCMD set {BVariable}=True\x0a', 'utf-8'), (RR150, 7700))
      if values['-WP-'] == True:
        sock.sendto(bytes(f'SCMD set {BVariable}=True\x0a', 'utf-8'), (RRWP, 7700))
      if values['-Cubes-'] == True:
        sock.sendto(bytes(f'SCMD set {BVariable}=True\x0a', 'utf-8'), (RRCUBA, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=True\x0a', 'utf-8'), (RRCUBB, 7700))
      if values['-TOTH-'] == True:
        sock.sendto(bytes(f'SCMD set {BVariable}=True\x0a', 'utf-8'), (RRTOTH, 7700))
      if values['-Market-'] == True:
        sock.sendto(bytes(f'SCMD set {BVariable}=True\x0a', 'utf-8'), (RRMark, 7700))
      if values['-MemA-'] == True:
        sock.sendto(bytes(f'SCMD set {BVariable}=True\x0a', 'utf-8'), (RRMemA, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=True\x0a', 'utf-8'), (RRMemB, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=True\x0a', 'utf-8'), (RRMemC, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=True\x0a', 'utf-8'), (RRMemD, 7700))
      if values['-PubA-'] == True:
#        sock.sendto(bytes(f'SCMD set {BVariable}=True\x0a', 'utf-8'), (RRPubA, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=True\x0a', 'utf-8'), (RRPubB, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=True\x0a', 'utf-8'), (RRPubC, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=True\x0a', 'utf-8'), (RRPubD, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=True\x0a', 'utf-8'), (RRPubE, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=True\x0a', 'utf-8'), (RRPubF, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=True\x0a', 'utf-8'), (RRPubG, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=True\x0a', 'utf-8'), (RRPubH, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=True\x0a', 'utf-8'), (RRPubI, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=True\x0a', 'utf-8'), (RRPubJ, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=True\x0a', 'utf-8'), (RRPubK, 7700))

    #Rosehill    
    if values['-Rosehill-'] == True:
      if values['-150-'] == True:
        sock.sendto(bytes(f'SCMD set {BVariable}=True\x0a', 'utf-8'), (RG150, 7700))
      if values['-WP-'] == True:
        sock.sendto(bytes(f'SCMD set {BVariable}=True\x0a', 'utf-8'), (RGWP, 7700))
      if values['-Market-'] == True:
        sock.sendto(bytes(f'SCMD set {BVariable}=True\x0a', 'utf-8'), (RGMark, 7700))
      if values['-MemA-'] == True:
        sock.sendto(bytes(f'SCMD set {BVariable}=True\x0a', 'utf-8'), (RGMemA, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=True\x0a', 'utf-8'), (RGMemB, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=True\x0a', 'utf-8'), (RGMemC, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=True\x0a', 'utf-8'), (RGMemD, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=True\x0a', 'utf-8'), (RGMemE, 7700))
      if values['-PubA-'] == True:
        sock.sendto(bytes(f'SCMD set {BVariable}=True\x0a', 'utf-8'), (RGPubA, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=True\x0a', 'utf-8'), (RGPubB, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=True\x0a', 'utf-8'), (RGPubC, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=True\x0a', 'utf-8'), (RGPubD, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=True\x0a', 'utf-8'), (RGPubE, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=True\x0a', 'utf-8'), (RGPubF, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=True\x0a', 'utf-8'), (RGPubG, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=True\x0a', 'utf-8'), (RGPubH, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=True\x0a', 'utf-8'), (RGPubI, 7700))

  #Set variable, set text colour back to white to let us know it's not active anymore, and send the stop command.
  if event == 'Stop!':
    if values['-R1S-'] == True:
      #window.FindElement('-R1S-').Update(text_color = 'white'),
      window['-R1S-'].Widget.config(fg='white'),
      BVariable = 'Race1'
    if values['-R2S-'] == True:
      window['-R2S-'].Widget.config(fg='white'),
      BVariable = 'Race2'
    if values['-R3S-'] == True:
      window['-R3S-'].Widget.config(fg='white'),
      BVariable = 'Race3'
    if values['-R4S-'] == True:
      window['-R4S-'].Widget.config(fg='white'),
      BVariable = 'Race4'
    if values['-R5S-'] == True:
      window['-R5S-'].Widget.config(fg='white'),
      BVariable = 'Race5'
    if values['-R6S-'] == True:
      window['-R6S-'].Widget.config(fg='white'),
      BVariable = 'Race6'
    if values['-R7S-'] == True:
      window['-R7S-'].Widget.config(fg='white'),
      BVariable = 'Race7'
    if values['-R8S-'] == True:
      window['-R8S-'].Widget.config(fg='white'),
      BVariable = 'Race8'
    if values['-R9S-'] == True:
      window['-R9S-'].Widget.config(fg='white'),
      BVariable = 'Race9'
    if values['-R10S-'] == True:
      window['-R10S-'].Widget.config(fg='white'),
      BVariable = 'Race10'
    if values['-B1-'] == True:
      window['-B1-'].Widget.config(fg='white'),
      BVariable = 'Boolean1'
    if values['-B2-'] == True:
      window['-B2-'].Widget.config(fg='white'),
      BVariable = 'Boolean2'
    if values['-B3-'] == True:
      window['-B3-'].Widget.config(fg='white'),
      BVariable = 'Boolean3'
    if values['-B4-'] == True:
      window['-B4-'].Widget.config(fg='white'),
      BVariable = 'Boolean4'
    if values['-PotY-'] == True:
      window['-PotY-'].Widget.config(fg='white'),
      BVariable = 'YardPick'
    if values['-EndDay-'] == True:
      window['-EndDay-'].Widget.config(fg='white'),
      BVariable = 'EndDay'
    if values['-Track-'] == True:
      window['-Track-'].Widget.config(fg='white'),
      BVariable = 'Track'
    if values['-TestGrid-'] == True:
      window['-TestGrid-'].Widget.config(fg='white'),
      BVariable = 'TestGrid'

    #Now things get annoying, send the trigger to the right IP address
    #Randwick
    if values['-Randwick-'] == True:
      if values['-150-'] == True:
        sock.sendto(bytes(f'SCMD set {BVariable}=False\x0a', 'utf-8'), (RR150, 7700))
      if values['-WP-'] == True:
        sock.sendto(bytes(f'SCMD set {BVariable}=False\x0a', 'utf-8'), (RRWP, 7700))
      if values['-Cubes-'] == True:
        sock.sendto(bytes(f'SCMD set {BVariable}=False\x0a', 'utf-8'), (RRCUBA, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=False\x0a', 'utf-8'), (RRCUBB, 7700))
      if values['-TOTH-'] == True:
        sock.sendto(bytes(f'SCMD set {BVariable}=False\x0a', 'utf-8'), (RRTOTH, 7700))
      if values['-Market-'] == True:
        sock.sendto(bytes(f'SCMD set {BVariable}=False\x0a', 'utf-8'), (RRMark, 7700))
      if values['-MemA-'] == True:
        sock.sendto(bytes(f'SCMD set {BVariable}=False\x0a', 'utf-8'), (RRMemA, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=False\x0a', 'utf-8'), (RRMemB, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=False\x0a', 'utf-8'), (RRMemC, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=False\x0a', 'utf-8'), (RRMemD, 7700))
      if values['-PubA-'] == True:
#        sock.sendto(bytes(f'SCMD set {BVariable}=False\x0a', 'utf-8'), (RRPubA, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=False\x0a', 'utf-8'), (RRPubB, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=False\x0a', 'utf-8'), (RRPubC, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=False\x0a', 'utf-8'), (RRPubD, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=False\x0a', 'utf-8'), (RRPubE, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=False\x0a', 'utf-8'), (RRPubF, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=False\x0a', 'utf-8'), (RRPubG, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=False\x0a', 'utf-8'), (RRPubH, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=False\x0a', 'utf-8'), (RRPubI, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=False\x0a', 'utf-8'), (RRPubJ, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=False\x0a', 'utf-8'), (RRPubK, 7700))

    #Rosehill    
    if values['-Rosehill-'] == True:
      if values['-150-'] == True:
        sock.sendto(bytes(f'SCMD set {BVariable}=False\x0a', 'utf-8'), (RG150, 7700))
      if values['-WP-'] == True:
        sock.sendto(bytes(f'SCMD set {BVariable}=False\x0a', 'utf-8'), (RGWP, 7700))
      if values['-Market-'] == True:
        sock.sendto(bytes(f'SCMD set {BVariable}=False\x0a', 'utf-8'), (RGMark, 7700))
      if values['-MemA-'] == True:
        sock.sendto(bytes(f'SCMD set {BVariable}=False\x0a', 'utf-8'), (RGMemA, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=False\x0a', 'utf-8'), (RGMemB, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=False\x0a', 'utf-8'), (RGMemC, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=False\x0a', 'utf-8'), (RGMemD, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=False\x0a', 'utf-8'), (RGMemE, 7700))
      if values['-PubA-'] == True:
        sock.sendto(bytes(f'SCMD set {BVariable}=False\x0a', 'utf-8'), (RGPubA, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=False\x0a', 'utf-8'), (RGPubB, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=False\x0a', 'utf-8'), (RGPubC, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=False\x0a', 'utf-8'), (RGPubD, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=False\x0a', 'utf-8'), (RGPubE, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=False\x0a', 'utf-8'), (RGPubF, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=False\x0a', 'utf-8'), (RGPubG, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=False\x0a', 'utf-8'), (RGPubH, 7700))
        sock.sendto(bytes(f'SCMD set {BVariable}=False\x0a', 'utf-8'), (RGPubI, 7700))

  #Integer Triggers
  if event == 'Go!':
    if values['-PoTH-'] == True:
      IVariable = 'PoTH'
    if values['-Marketing-'] == True:
      IVariable = 'Pixeloco'
    if values['-BCW-'] == True:
      IVariable = 'Bowermans_CW'
    if values['-ESSCW-'] == True:
      IVariable = 'ESS_CW'
    if values['-TVC-'] == True:
      IVariable = 'TVC'
    if values['-DayHype-'] == True:
      IVariable = 'DayHype'
    if values['-FSA-'] == True:
      IVariable = 'Fashion'
    if values['-FS1-'] == True:
      IVariable = 'Hour1'
    if values['-FS2-'] == True:
      IVariable = 'Hour2'
    if values['-T1-'] == True:
      IVariable = 'Trigger1'
    if values['-T2-'] == True:
      IVariable = 'Trigger2'
    if values['-T3-'] == True:
      IVariable = 'Trigger3'
    if values['-T4-'] == True:
      IVariable = 'Trigger4'
	  #Now things get annoying, send the trigger to the right IP address
    #Randwick
    if values['-Randwick-'] == True:
      if values['-150-'] == True:
        sock.sendto(bytes(f'SCMD increment {IVariable}\x0a', 'utf-8'), (RR150, 7700))
      if values['-WP-'] == True:
        sock.sendto(bytes(f'SCMD increment {IVariable}\x0a', 'utf-8'), (RRWP, 7700))
      if values['-Cubes-'] == True:
        sock.sendto(bytes(f'SCMD increment {IVariable}\x0a', 'utf-8'), (RRCUBA, 7700))
        sock.sendto(bytes(f'SCMD increment {IVariable}\x0a', 'utf-8'), (RRCUBB, 7700))
      if values['-TOTH-'] == True:
        sock.sendto(bytes(f'SCMD increment {IVariable}\x0a', 'utf-8'), (RRTOTH, 7700))
      if values['-Market-'] == True:
        sock.sendto(bytes(f'SCMD increment {IVariable}\x0a', 'utf-8'), (RRMark, 7700))
      if values['-MemA-'] == True:
        sock.sendto(bytes(f'SCMD increment {IVariable}\x0a', 'utf-8'), (RRMemA, 7700))
        sock.sendto(bytes(f'SCMD increment {IVariable}\x0a', 'utf-8'), (RRMemB, 7700))
        sock.sendto(bytes(f'SCMD increment {IVariable}\x0a', 'utf-8'), (RRMemC, 7700))
        sock.sendto(bytes(f'SCMD increment {IVariable}\x0a', 'utf-8'), (RRMemD, 7700))
      if values['-PubA-'] == True:
#        sock.sendto(bytes(f'SCMD increment {IVariable}\x0a', 'utf-8'), (RRPubA, 7700))
        sock.sendto(bytes(f'SCMD increment {IVariable}\x0a', 'utf-8'), (RRPubB, 7700))
        sock.sendto(bytes(f'SCMD increment {IVariable}\x0a', 'utf-8'), (RRPubC, 7700))
        sock.sendto(bytes(f'SCMD increment {IVariable}\x0a', 'utf-8'), (RRPubD, 7700))
        sock.sendto(bytes(f'SCMD increment {IVariable}\x0a', 'utf-8'), (RRPubE, 7700))
        sock.sendto(bytes(f'SCMD increment {IVariable}\x0a', 'utf-8'), (RRPubF, 7700))
        sock.sendto(bytes(f'SCMD increment {IVariable}\x0a', 'utf-8'), (RRPubG, 7700))
        sock.sendto(bytes(f'SCMD increment {IVariable}\x0a', 'utf-8'), (RRPubH, 7700))
        sock.sendto(bytes(f'SCMD increment {IVariable}\x0a', 'utf-8'), (RRPubI, 7700))
        sock.sendto(bytes(f'SCMD increment {IVariable}\x0a', 'utf-8'), (RRPubJ, 7700))
        sock.sendto(bytes(f'SCMD increment {IVariable}\x0a', 'utf-8'), (RRPubK, 7700))

    #Rosehill    
    if values['-Rosehill-'] == True:
      if values['-150-'] == True:
        sock.sendto(bytes(f'SCMD increment {IVariable}\x0a', 'utf-8'), (RG150, 7700))
      if values['-WP-'] == True:
        sock.sendto(bytes(f'SCMD increment {IVariable}\x0a', 'utf-8'), (RGWP, 7700))
      if values['-Market-'] == True:
        sock.sendto(bytes(f'SCMD increment {IVariable}\x0a', 'utf-8'), (RGMark, 7700))
      if values['-MemA-'] == True:
        sock.sendto(bytes(f'SCMD increment {IVariable}\x0a', 'utf-8'), (RGMemA, 7700))
        sock.sendto(bytes(f'SCMD increment {IVariable}\x0a', 'utf-8'), (RGMemB, 7700))
        sock.sendto(bytes(f'SCMD increment {IVariable}\x0a', 'utf-8'), (RGMemC, 7700))
        sock.sendto(bytes(f'SCMD increment {IVariable}\x0a', 'utf-8'), (RGMemD, 7700))
        sock.sendto(bytes(f'SCMD increment {IVariable}\x0a', 'utf-8'), (RGMemE, 7700))
      if values['-PubA-'] == True:
        sock.sendto(bytes(f'SCMD increment {IVariable}\x0a', 'utf-8'), (RGPubA, 7700))
        sock.sendto(bytes(f'SCMD increment {IVariable}\x0a', 'utf-8'), (RGPubB, 7700))
        sock.sendto(bytes(f'SCMD increment {IVariable}\x0a', 'utf-8'), (RGPubC, 7700))
        sock.sendto(bytes(f'SCMD increment {IVariable}\x0a', 'utf-8'), (RGPubD, 7700))
        sock.sendto(bytes(f'SCMD increment {IVariable}\x0a', 'utf-8'), (RGPubE, 7700))
        sock.sendto(bytes(f'SCMD increment {IVariable}\x0a', 'utf-8'), (RGPubF, 7700))
        sock.sendto(bytes(f'SCMD increment {IVariable}\x0a', 'utf-8'), (RGPubG, 7700))
        sock.sendto(bytes(f'SCMD increment {IVariable}\x0a', 'utf-8'), (RGPubH, 7700))
        sock.sendto(bytes(f'SCMD increment {IVariable}\x0a', 'utf-8'), (RGPubI, 7700))


window.Close()
